# Trello Clone

This is a Trello clone project built using React, designed to provide a user-friendly way to manage tasks and projects using a board-based interface.


## GitLab Repository

You can find the project's source code on GitHub at [https://gitlab.com/Vivek03t/trello-react-vivek](https://gitlab.com/Vivek03t/trello-react-vivek).

Switch branches to see the useReducer and redux implementation.

## Features

- Create, and display boards.
- Display, create, and delete lists within boards.
- Add and organize tasks in each list.
- Display, create, and delete checklists.
- Display, create, delete, check and uncheck a checkitem.
- User-friendly and responsive design.

## Technologies Used

- [React](https://reactjs.org/)
- [Vite](https://vitejs.dev/)
- [Material-UI](https://material-ui.com/): A popular React UI framework.
- HTML
- CSS
- JavaScript


## Getting Started

To get a local copy of this project up and running, follow these steps:

1. **Clone the repository:**

   ```bash
   git clone https://gitlab.com/Vivek03t/trello-react-vivek
   ```
2. **Navigate to the project directory:**
   ```bash
   cd trello-react-vivek
   ```
3. **Install dependencies:**
     ```bash
     npm install
     ```
4. **Run the development server:**
    ```bash
    npm run dev
    ```
5. **Open your browser:**
   Your app should now be running at http://localhost:5173.


## Usage

- On the homepage, you can see all Boards.
- You can create a new board by clicking on the create new board. 
- Click on a board to see all the lists and it's cards.
- You can create and delete a list.
- You can add and remove a card from the list.
- You can click on a card to see it's checklist and checkitem.
- You can create, delete and see the progress of checklist.
- You can create, delete, check nand uncheck the checkitem.
  
## Acknowledgments

Thanks to Trello for providing the Trello API used in this project.  