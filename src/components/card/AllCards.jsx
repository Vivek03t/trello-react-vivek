import React, { useEffect, useState } from 'react';
import Box from '@mui/material/Box';
import Stack from '@mui/material/Stack';
import axios from 'axios';
import { PropTypes } from 'prop-types';
import CreateNewCard from './CreateNewCard';
import CardModal from './CardModal';

const cardStyle = {
  display: 'flex',
  width: '15rem',
  justifyContent: 'center',
  marginBottom: '1rem',
};

export default function AllCards({ list }) {
  const id = list.id;

  const [allCards, setAllCards] = useState([]);

  useEffect(() => {
    axios
      .get(
        `${import.meta.env.VITE_SINGLE_LIST_URL}/${id}/cards?key=${
          import.meta.env.VITE_API_KEY
        }&token=${import.meta.env.VITE_API_TOKEN}`,
      )
      .then((response) => {
        setAllCards(response.data);
      })
      .catch((error) => {
        console.log('An error occurred:', error);
      });
  }, [id]);

  return (
    <Box sx={cardStyle}>
      <Stack spacing={2} width={'13rem'}>
        {allCards.map((card) => (
          <CardModal
            key={card.id}
            card={card}
            list={list}
            allCards={allCards}
            setAllCards={setAllCards}
          />
        ))}
        <CreateNewCard id={id} allCards={allCards} setAllCards={setAllCards} />
      </Stack>
    </Box>
  );
}

AllCards.propTypes = {
  list: PropTypes.object.isRequired,
};
