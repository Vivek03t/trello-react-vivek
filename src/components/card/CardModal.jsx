import React, { useState } from 'react';
import AllChecklists from '../checklist/AllChecklists';
import DeleteCard from './DeleteCard';
import Modal from '@mui/material/Modal';
import Paper from '@mui/material/Paper';
import { styled } from '@mui/material/styles';
import { PropTypes } from 'prop-types';

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'center',
  color: theme.palette.text.secondary,
}));

export default function CardModal({ list, card, allCards, setAllCards }) {
  const [open, setOpen] = useState(false);

  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  return (
    <>
      <Item
        sx={{ display: 'flex', justifyContent: 'space-between' }}
        key={card.id}
        style={{
          backgroundColor: 'var(--Card-BgColor)',
          color: 'var(--Text-Color)',
          borderRadius: '10px',
        }}
        onClick={() => {
          handleOpen();
        }}
      >
        {card.name}
        <DeleteCard
          cardId={card.id}
          allCards={allCards}
          setAllCards={setAllCards}
        />
      </Item>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <AllChecklists
          list={list}
          card={card}
          handleOpen={handleOpen}
          handleClose={handleClose}
        />
      </Modal>
    </>
  );
}

CardModal.propTypes = {
  list: PropTypes.object.isRequired,
  card: PropTypes.object.isRequired,
  allCards: PropTypes.array.isRequired,
  setAllCards: PropTypes.func.isRequired,
};
