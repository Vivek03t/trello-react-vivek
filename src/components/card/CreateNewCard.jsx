import React, { useState } from 'react';
import {
  Card,
  CardContent,
  Typography,
  CardActionArea,
  Button,
} from '@mui/material';
import { PropTypes } from 'prop-types';
import axios from 'axios';

export default function CreateNewCard({ id, allCards, setAllCards }) {
  const [isFormOpen, setIsFormOpen] = useState(false);
  const [cardName, setCardName] = useState('');

  const cardStyle = {
    maxWidth: 345,
    backgroundColor: 'transparent',
    borderRadius: '12px',
  };

  const contentStyle = {
    height: '2rem',
    display: 'flex',
    alignItems: 'center',
  };

  const nameStyle = {
    color: 'var(--Text-Color)',
    fontSize: '10px',
  };

  const handleCreateCard = () => {
    if (cardName.trim() !== '') {
      axios
        .post(
          `${import.meta.env.VITE_CARD_URL}?idList=${id}&key=${
            import.meta.env.VITE_API_KEY
          }&token=${import.meta.env.VITE_API_TOKEN}`,
          {
            name: cardName,
          },
        )
        .then((response) => {
          setAllCards([...allCards, response.data]);
          setCardName('');
        })
        .catch((error) => {
          console.error('An error occurred:', error);
        });
    }
  };

  return (
    <Card style={cardStyle}>
      {isFormOpen ? (
        <CardContent>
          <input
            type="text"
            placeholder="Enter a title for this card..."
            value={cardName}
            onChange={(e) => setCardName(e.target.value)}
            style={{ height: '2rem', marginBottom: '10px' }}
          />
          <Button
            variant="contained"
            color="primary"
            onClick={() => {
              handleCreateCard();
              setIsFormOpen(false);
            }}
          >
            Add card
          </Button>
          <Button
            variant="contained"
            color="secondary"
            onClick={() => setIsFormOpen(false)}
          >
            Cancel
          </Button>
        </CardContent>
      ) : (
        <CardActionArea
          onClick={() => setIsFormOpen(true)}
          onMouseEnter={(e) =>
            (e.target.style.backgroundColor = 'var(--Card-BgColor)')
          }
          onMouseLeave={(e) => (e.target.style.backgroundColor = 'inherit')}
        >
          <CardContent style={contentStyle}>
            <Typography variant="h6" component="div" style={nameStyle}>
              + Add a card
            </Typography>
          </CardContent>
        </CardActionArea>
      )}
    </Card>
  );
}

CreateNewCard.propTypes = {
  id: PropTypes.string.isRequired,
  allCards: PropTypes.array.isRequired,
  setAllCards: PropTypes.func.isRequired,
};
