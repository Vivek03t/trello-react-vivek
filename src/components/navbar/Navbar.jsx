import React from 'react';
import { useNavigate } from 'react-router-dom';
import { styled, alpha } from '@mui/material/styles';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import InputBase from '@mui/material/InputBase';
import MenuIcon from '@mui/icons-material/Menu';
import SearchIcon from '@mui/icons-material/Search';
import Button from '@mui/material/Button';

const Search = styled('div')(({ theme }) => ({
  position: 'relative',
  borderRadius: theme.shape.borderRadius,
  backgroundColor: alpha(theme.palette.common.white, 0.15),
  '&:hover': {
    backgroundColor: alpha(theme.palette.common.white, 0.25),
  },
  marginLeft: 0,
  width: '100%',
  [theme.breakpoints.up('sm')]: {
    marginLeft: theme.spacing(1),
    width: 'auto',
  },
}));

const SearchIconWrapper = styled('div')(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: '100%',
  position: 'absolute',
  pointerEvents: 'none',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: 'inherit',
  '& .MuiInputBase-input': {
    padding: theme.spacing(1, 1, 1, 0),
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: '12ch',
      '&:focus': {
        width: '20ch',
      },
    },
  },
}));

export default function Navbar() {
  const navigate = useNavigate();

  const handleLogoClick = () => {
    navigate(`/`);
  };

  const StyledLogo = {
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'contain',
    backgroundPposition: 'center',
    display: 'block',
    position: 'absolute',
    top: '12px',
  };

  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static" style={{ background: 'var(--Navbar-BgColor)' }}>
        <Toolbar>
          <IconButton
            size="medium"
            edge="start"
            color="inherit"
            aria-label="open drawer"
            sx={{ mr: 1 }}
          >
            <MenuIcon />
          </IconButton>
          <div style={{ order: 1 }}>
            <Typography
              variant="h6"
              noWrap
              component="div"
              sx={{ display: { xs: 'none', sm: 'block' } }}
            >
              <div style={StyledLogo}>
                <img
                  src="/src/assets/trello-logo.gif"
                  style={{
                    width: '25%',
                    height: '10%',
                    color: 'var(--Text-Color)',
                    cursor: 'pointer',
                  }}
                  onClick={handleLogoClick}
                  alt="trello-logo"
                />
              </div>
            </Typography>
          </div>
          <div
            style={{
              display: 'flex',
              order: 2,
              marginLeft: '7%',
              color: 'var(--Text-Color)',
            }}
          >
            <Button color="inherit" style={{ textTransform: 'none' }}>
              Workspaces
            </Button>
            <Button color="inherit" style={{ textTransform: 'none' }}>
              Recent
            </Button>
            <Button color="inherit" style={{ textTransform: 'none' }}>
              Starred
            </Button>
          </div>
          <Button
            variant="contained"
            color="primary"
            style={{
              order: 2,
              textTransform: 'none',
              color: 'var(--Text-Color)',
            }}
          >
            Create
          </Button>

          <Search
            style={{ order: 3, marginLeft: 'auto', color: 'var(--Text-Color)' }}
          >
            <SearchIconWrapper>
              <SearchIcon />
            </SearchIconWrapper>
            <StyledInputBase
              placeholder="Search…"
              inputProps={{ 'aria-label': 'search' }}
            />
          </Search>
        </Toolbar>
      </AppBar>
    </Box>
  );
}
