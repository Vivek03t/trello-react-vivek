import React, { useState } from 'react';
import DeleteIcon from '@mui/icons-material/Delete';
import axios from 'axios';
import { PropTypes } from 'prop-types';

export default function DeleteList({ list, allLists, setAllLists }) {
  const [isHovered, setIsHovered] = useState(false);

  const handleDelete = (id) => {
    axios
      .put(
        `${import.meta.env.VITE_SINGLE_LIST_URL}/${id}/closed?key=${
          import.meta.env.VITE_API_KEY
        }&token=${import.meta.env.VITE_API_TOKEN}&value=true`,
      )
      .then(() => {
        const updatedList = allLists.filter((list) => list.id !== id);
        setAllLists(updatedList);
      })
      .catch((error) => {
        console.error('An error occurred:', error);
      });
  };

  const iconStyle = {
    color: isHovered ? 'Crimson' : 'inherit',
    cursor: 'pointer',
  };

  return (
    <DeleteIcon
      style={iconStyle}
      onClick={() => handleDelete(list.id)}
      onMouseEnter={() => setIsHovered(true)}
      onMouseLeave={() => setIsHovered(false)}
    />
  );
}

DeleteList.propTypes = {
  allLists: PropTypes.array.isRequired,
  setAllLists: PropTypes.func.isRequired,
  list: PropTypes.object.isRequired,
};
