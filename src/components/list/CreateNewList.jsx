import React, { useState } from 'react';
import {
  Card,
  CardContent,
  Typography,
  CardActionArea,
  Button,
} from '@mui/material';
import { PropTypes } from 'prop-types';
import axios from 'axios';
import { useParams } from 'react-router-dom';

export default function CreateNewList({ allLists, setAllLists }) {
  const { BoardId } = useParams();

  const [isFormOpen, setIsFormOpen] = useState(false);
  const [listName, setListName] = useState('');

  const cardStyle = {
    maxWidth: 345,
    backgroundColor: 'var(--Dark-Blue)',
    borderRadius: '12px',
  };

  const contentStyle = {
    width: '15rem',
    height: '3rem',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  };

  const nameStyle = {
    color: 'var(--Text-Color)',
    fontSize: '10px',
  };

  const handleCreateList = () => {
    if (listName.trim() !== '') {
      axios
        .post(
          `${
            import.meta.env.VITE_LIST_URL
          }${BoardId}/lists?name=${listName}&key=${
            import.meta.env.VITE_API_KEY
          }&token=${import.meta.env.VITE_API_TOKEN}`,
          {
            name: listName,
          },
        )
        .then((response) => {
          setAllLists([...allLists, response.data]);
          setListName('');
        })
        .catch((error) => {
          console.error('An error occurred:', error);
        });
    }
  };
  return (
    <>
      <Card style={cardStyle}>
        {isFormOpen ? (
          <CardContent>
            <input
              type="text"
              placeholder="Enter list title..."
              value={listName}
              onChange={(e) => setListName(e.target.value)}
              style={{ height: '2rem', marginBottom: '10px' }}
            />
            <Button
              variant="contained"
              color="primary"
              onClick={() => {
                handleCreateList();
                setIsFormOpen(false);
              }}
            >
              Add list
            </Button>
            <Button
              variant="contained"
              color="secondary"
              onClick={() => setIsFormOpen(false)}
            >
              Cancel
            </Button>
          </CardContent>
        ) : (
          <CardActionArea onClick={() => setIsFormOpen(true)}>
            <CardContent style={contentStyle}>
              <Typography variant="h6" component="div" style={nameStyle}>
                + Add another list
              </Typography>
            </CardContent>
          </CardActionArea>
        )}
      </Card>
    </>
  );
}

CreateNewList.propTypes = {
  allLists: PropTypes.array.isRequired,
  setAllLists: PropTypes.func.isRequired,
};
