import * as React from 'react';
import Box from '@mui/material/Box';
import { Card, CardContent, Typography } from '@mui/material';
import { PropTypes } from 'prop-types';
import DeleteList from './DeleteList';
import AllCards from '../card/AllCards';

export default function ListCard({ listName, list, allLists, setAllLists }) {
  return (
    <>
      <Box
        width="auto"
        minWidth={300}
        maxWidth={500}
        borderRadius={'20px'}
        style={{ height: '98vh' }}
      >
        <Card
          sx={{
            backgroundColor: 'var(--List-BgColor)',
            color: 'var(--Text-Color)',
            borderRadius: '14px',
          }}
        >
          <CardContent
            sx={{ display: 'flex', justifyContent: 'space-between' }}
          >
            <Typography
              component="h2"
              variant="h6"
              marginBottom={2}
              fontSize={'20px'}
            >
              {listName}
            </Typography>
            <DeleteList
              list={list}
              allLists={allLists}
              setAllLists={setAllLists}
            />
          </CardContent>
          <AllCards list={list} />
        </Card>
      </Box>
    </>
  );
}

ListCard.propTypes = {
  listName: PropTypes.string.isRequired,
  allLists: PropTypes.array.isRequired,
  setAllLists: PropTypes.func.isRequired,
  list: PropTypes.object.isRequired,
};
