import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { useParams } from 'react-router-dom';
import ListCard from '../list/ListCard';
import { Stack } from '@mui/material';
import CreateNewList from '../list/CreateNewList';
import Loader from '../helper/Loader';

export default function ListPage() {
  const { BoardId } = useParams();
  const [allLists, setAllLists] = useState([]);
  const [isLoading, setLoader] = useState(true);
  const [hasError, setError] = useState('');

  useEffect(() => {
    setLoader(true);
    axios
      .get(
        `${import.meta.env.VITE_LIST_URL}${BoardId}/lists?key=${
          import.meta.env.VITE_API_KEY
        }&token=${import.meta.env.VITE_API_TOKEN}`,
      )
      .then((response) => {
        setAllLists(response.data);
        setTimeout(() => {
          setLoader(false);
        }, 1000);
      })
      .catch((error) => {
        console.log('An error occurred:', error);
        setError(error);
      });
  }, [BoardId]);

  return (
    <div className="listCard__container">
      {hasError ? (
        <div className="error-message">
          <p>An error occurred: {hasError.message}</p>
        </div>
      ) : isLoading ? (
        <Loader />
      ) : (
        <Stack
          container
          spacing={2}
          direction={'row'}
          sx={{ overflowX: 'scroll', overflowY: 'hidden' }}
        >
          {allLists.map((list) => (
            <Stack item xs={12} sm={6} md={4} lg={3} key={list.id}>
              <ListCard
                listName={list.name}
                list={list}
                allLists={allLists}
                setAllLists={setAllLists}
              />
            </Stack>
          ))}
          <Stack item xs={12} sm={6} md={4} lg={3}>
            <CreateNewList allLists={allLists} setAllLists={setAllLists} />
          </Stack>
        </Stack>
      )}
    </div>
  );
}
