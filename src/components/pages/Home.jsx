import React from 'react';
import AllBoards from '../board/AllBoards';

export default function Home() {
  return (
    <div className="main__container">
      <AllBoards />
    </div>
  );
}
