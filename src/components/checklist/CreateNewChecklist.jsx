import React, { useState } from 'react';
import {
  Card,
  CardContent,
  Typography,
  CardActionArea,
  Button,
} from '@mui/material';
import { PropTypes } from 'prop-types';
import axios from 'axios';

export default function CreateNewChecklist({
  cardId,
  allChecklists,
  setAllChecklists,
}) {
  const [isFormOpen, setIsFormOpen] = useState(false);
  const [checklistName, setChecklistName] = useState('');

  const cardStyle = {
    maxWidth: 345,
    backgroundColor: 'hsl(209, 23%, 22%)',
    borderRadius: '12px',
  };

  const contentStyle = {
    width: '15rem',
    height: '3rem',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  };

  const nameStyle = {
    color: 'var(--Text-Color)',
    fontSize: '10px',
  };

  const handleCreateChecklist = () => {
    if (checklistName.trim() !== '') {
      axios
        .post(
          `${
            import.meta.env.VITE_CHECKLIST_URL
          }?idCard=${cardId}&name=${checklistName}&key=${
            import.meta.env.VITE_API_KEY
          }&token=${import.meta.env.VITE_API_TOKEN}`,
          {
            name: checklistName,
          },
        )
        .then((response) => {
          setAllChecklists([...allChecklists, response.data]);
          setChecklistName('');
        })
        .catch((error) => {
          console.error('An error occurred:', error);
        });
    }
  };
  return (
    <>
      <Card style={cardStyle}>
        {isFormOpen ? (
          <CardContent>
            <input
              type="text"
              placeholder="Enter title"
              value={checklistName}
              onChange={(e) => setChecklistName(e.target.value)}
              style={{ height: '2rem', marginBottom: '10px' }}
            />
            <Button
              variant="contained"
              color="primary"
              onClick={() => {
                handleCreateChecklist();
                setIsFormOpen(false);
              }}
            >
              Add checklist
            </Button>
            <Button
              variant="contained"
              color="secondary"
              onClick={() => setIsFormOpen(false)}
            >
              Cancel
            </Button>
          </CardContent>
        ) : (
          <CardActionArea onClick={() => setIsFormOpen(true)}>
            <CardContent style={contentStyle}>
              <Typography variant="h6" component="div" style={nameStyle}>
                + Add checklist
              </Typography>
            </CardContent>
          </CardActionArea>
        )}
      </Card>
    </>
  );
}

CreateNewChecklist.propTypes = {
  cardId: PropTypes.string.isRequired,
  allChecklists: PropTypes.array.isRequired,
  setAllChecklists: PropTypes.func.isRequired,
};
