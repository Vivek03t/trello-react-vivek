import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { PropTypes } from 'prop-types';
import { Card, CardContent, Typography, Box } from '@mui/material';
import DeleteChecklist from './DeleteChecklist';
import CloseIcon from '@mui/icons-material/Close';
import CreateNewChecklist from './CreateNewChecklist';
import AllCheckitems from '../checkitem/AllCheckitems';
import ChecklistIcon from '@mui/icons-material/Checklist';
import Loader from '../helper/Loader';

const boxStyle = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: '50%',
  height: 750,
  bgcolor: 'var(--Dark-Blue)',
  border: '2px solid #000',
  borderRadius: '12px',
  boxShadow: 24,
  p: 4,
};

export default function AllChecklists({ list, card, handleClose }) {
  const cardId = card.id;
  const [allChecklists, setAllChecklists] = useState([]);
  const [isLoading, setLoader] = useState(true);

  useEffect(() => {
    setLoader(true);
    axios
      .get(
        `${import.meta.env.VITE_CARD_URL}/${cardId}/checklists?key=${
          import.meta.env.VITE_API_KEY
        }&token=${import.meta.env.VITE_API_TOKEN}`,
      )
      .then((response) => {
        setAllChecklists(response.data);
        setTimeout(() => {
          setLoader(false);
        }, 1000);
      })
      .catch((error) => {
        console.log('An error occurred:', error);
      });
  }, [cardId]);

  return (
    <div>
      {isLoading ? (
        <Loader />
      ) : (
        <Box sx={boxStyle}>
          <CloseIcon
            onClick={() => handleClose()}
            style={{
              color: 'var(--Text-Color)',
              position: 'absolute',
              right: '30',
            }}
          />
          <div
            style={{
              color: 'var(--Text-Color)',
              display: 'flex',
              flexDirection: 'column',
              gap: '10px',
            }}
          >
            <span style={{ fontWeight: '700' }}>{card.name}</span> in list
            {' ' + list.name}
          </div>

          {allChecklists.map((checklist) => (
            <Card
              key={checklist.id}
              sx={{
                backgroundColor: 'var(--Dark-Blue)',
                color: 'var(--Text-Color)',
                borderRadius: '14px',
                cursor: 'pointer',
                // width: '30rem',
                height: 'auto',
                marginBottom: '1rem',
                marginTop: '1rem',
              }}
            >
              <CardContent
                sx={{
                  display: 'flex',
                  justifyContent: 'space-between',
                  flexDirection: 'row-reverse',
                  padding: '5px',
                }}
              >
                <DeleteChecklist
                  checklistId={checklist.id}
                  allChecklists={allChecklists}
                  setAllChecklists={setAllChecklists}
                />
                <div
                  style={{
                    display: 'flex',
                    gap: '1rem',
                  }}
                >
                  <ChecklistIcon />
                  <Typography
                    component="h2"
                    variant="h6"
                    marginBottom={2}
                    fontSize={'20px'}
                  >
                    {checklist.name}
                  </Typography>
                </div>
              </CardContent>
              <AllCheckitems checklistId={checklist.id} cardId={cardId} />
            </Card>
          ))}
          <CreateNewChecklist
            cardId={cardId}
            allChecklists={allChecklists}
            setAllChecklists={setAllChecklists}
          />
        </Box>
      )}
    </div>
  );
}

AllChecklists.propTypes = {
  list: PropTypes.object.isRequired,
  card: PropTypes.object.isRequired,
  handleOpen: PropTypes.func.isRequired,
  handleClose: PropTypes.func.isRequired,
};
