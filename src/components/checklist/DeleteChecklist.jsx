import React, { useState } from 'react';
import DeleteIcon from '@mui/icons-material/Delete';
import axios from 'axios';
import { PropTypes } from 'prop-types';

export default function DeleteChecklist({
  checklistId,
  allChecklists,
  setAllChecklists,
}) {
  const [isHovered, setIsHovered] = useState(false);

  const handleDelete = (id) => {
    axios
      .delete(
        `${import.meta.env.VITE_CHECKLIST_URL}/${id}?key=${
          import.meta.env.VITE_API_KEY
        }&token=${import.meta.env.VITE_API_TOKEN}`,
      )
      .then((response) => {
        console.log(response.data);
        const updatedChecklists = allChecklists.filter(
          (checklist) => checklist.id !== id,
        );
        setAllChecklists(updatedChecklists);
      })
      .catch((error) => {
        console.error('An error occurred:', error);
      });
  };

  const iconStyle = {
    color: isHovered ? 'Crimson' : 'inherit',
    cursor: 'pointer',
  };

  return (
    <DeleteIcon
      style={iconStyle}
      onClick={() => handleDelete(checklistId)}
      onMouseEnter={() => setIsHovered(true)}
      onMouseLeave={() => setIsHovered(false)}
    />
  );
}

DeleteChecklist.propTypes = {
  allChecklists: PropTypes.array.isRequired,
  setAllChecklists: PropTypes.func.isRequired,
  checklistId: PropTypes.string.isRequired,
};
