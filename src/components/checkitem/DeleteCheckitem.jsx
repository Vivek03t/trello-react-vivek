import React, { useState } from 'react';
import DeleteIcon from '@mui/icons-material/Delete';
import axios from 'axios';
import { PropTypes } from 'prop-types';

export default function DeleteCheckitem({
  checklistId,
  checkitemId,
  allCheckitems,
  setAllCheckitems,
}) {
  const [isHovered, setIsHovered] = useState(false);

  const handleDelete = (id) => {
    axios
      .delete(
        `${
          import.meta.env.VITE_CHECKLIST_URL
        }/${checklistId}/checkItems/${checkitemId}?key=${
          import.meta.env.VITE_API_KEY
        }&token=${import.meta.env.VITE_API_TOKEN}`,
      )
      .then(() => {
        const updatedCheckitems = allCheckitems.filter(
          (checkitem) => checkitem.id !== id,
        );
        setAllCheckitems(updatedCheckitems);
      })
      .catch((error) => {
        console.error('An error occurred:', error);
      });
  };

  const iconStyle = {
    color: isHovered ? 'Crimson' : 'inherit',
    cursor: 'pointer',
  };

  return (
    <DeleteIcon
      style={iconStyle}
      onClick={() => handleDelete(checkitemId)}
      onMouseEnter={() => setIsHovered(true)}
      onMouseLeave={() => setIsHovered(false)}
    />
  );
}

DeleteCheckitem.propTypes = {
  checklistId: PropTypes.string.isRequired,
  checkitemId: PropTypes.string.isRequired,
  allCheckitems: PropTypes.array.isRequired,
  setAllCheckitems: PropTypes.func.isRequired,
};
