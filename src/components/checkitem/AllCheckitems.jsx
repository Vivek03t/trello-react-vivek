import React, { useState, useEffect } from 'react';
import axios from 'axios';
import CreateNewCheckitem from './CreateNewCheckitem';
import { PropTypes } from 'prop-types';
import Progressbar from '../helper/Progressbar';
import ToggleCheckbox from '../checkbox/ToggleCheckbox';
import DeleteCheckitem from './DeleteCheckitem';

export default function AllCheckitems({ checklistId, cardId }) {
  const [allCheckitems, setAllCheckitems] = useState([]);

  useEffect(() => {
    axios
      .get(
        `${import.meta.env.VITE_CHECKLIST_URL}/${checklistId}/checkItems?key=${
          import.meta.env.VITE_API_KEY
        }&token=${import.meta.env.VITE_API_TOKEN}`,
      )
      .then((response) => {
        setAllCheckitems(response.data);
      })
      .catch((error) => {
        console.log('An error occurred:', error);
      });
  }, [checklistId]);

  return (
    <div style={{ marginLeft: '16px' }}>
      <Progressbar allCheckitems={allCheckitems} />
      {allCheckitems.map((checkitem) => {
        return (
          <div
            key={checkitem.id}
            style={{
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'space-between',
              padding: '5px',
            }}
          >
            <ToggleCheckbox
              checkitem={checkitem}
              cardId={cardId}
              allCheckitems={allCheckitems}
              setAllCheckitems={setAllCheckitems}
            />
            <DeleteCheckitem
              checklistId={checklistId}
              checkitemId={checkitem.id}
              allCheckitems={allCheckitems}
              setAllCheckitems={setAllCheckitems}
            />
          </div>
        );
      })}
      <CreateNewCheckitem
        checklistId={checklistId}
        allCheckitems={allCheckitems}
        setAllCheckitems={setAllCheckitems}
      />
    </div>
  );
}

AllCheckitems.propTypes = {
  checklistId: PropTypes.string.isRequired,
  cardId: PropTypes.string.isRequired,
};
