import React, { useState } from 'react';
import {
  Card,
  CardContent,
  Typography,
  CardActionArea,
  Button,
} from '@mui/material';
import { PropTypes } from 'prop-types';
import axios from 'axios';

export default function CreateNewCheckitem({
  checklistId,
  allCheckitems,
  setAllCheckitems,
}) {
  const [isFormOpen, setIsFormOpen] = useState(false);
  const [checkitemName, setCheckitemName] = useState('');

  const cardStyle = {
    maxWidth: 250,
    // width: 150,
    height: 'auto',
    marginBottom: '10px',
    backgroundColor: 'var(--Very-Dark-Blue)',
    borderRadius: '5px',
  };

  const contentStyle = {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  };

  const nameStyle = {
    color: 'var(--Text-Color)',
    fontSize: '10px',
  };

  const handleCreateCheckitem = () => {
    if (checkitemName.trim() !== '') {
      axios
        .post(
          `${
            import.meta.env.VITE_CHECKLIST_URL
          }/${checklistId}/checkItems?name=${checkitemName}&key=${
            import.meta.env.VITE_API_KEY
          }&token=${import.meta.env.VITE_API_TOKEN}`,
          {
            name: checkitemName,
          },
        )
        .then((response) => {
          setAllCheckitems([...allCheckitems, response.data]);
          setCheckitemName('');
        })
        .catch((error) => {
          console.error('An error occurred:', error);
        });
    }
  };
  return (
    <>
      <Card style={cardStyle}>
        {isFormOpen ? (
          <CardContent>
            <input
              type="text"
              placeholder="Enter list title..."
              value={checkitemName}
              onChange={(e) => setCheckitemName(e.target.value)}
              style={{ height: '2rem', marginBottom: '10px' }}
            />
            <Button
              variant="contained"
              color="primary"
              onClick={() => {
                handleCreateCheckitem();
                setIsFormOpen(false);
              }}
            >
              Add
            </Button>
            <Button
              variant="contained"
              color="secondary"
              onClick={() => setIsFormOpen(false)}
            >
              Cancel
            </Button>
          </CardContent>
        ) : (
          <CardActionArea onClick={() => setIsFormOpen(true)}>
            <CardContent style={contentStyle}>
              <Typography variant="h6" component="div" style={nameStyle}>
                Add an item
              </Typography>
            </CardContent>
          </CardActionArea>
        )}
      </Card>
    </>
  );
}

CreateNewCheckitem.propTypes = {
  checklistId: PropTypes.string.isRequired,
  allCheckitems: PropTypes.array.isRequired,
  setAllCheckitems: PropTypes.func.isRequired,
};
