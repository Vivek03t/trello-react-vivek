import React, { useState } from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import { CardActionArea } from '@mui/material';
import Button from '@mui/material/Button';
import PropTypes from 'prop-types';
import axios from 'axios';
import AllBoards from './AllBoards';

export default function CreateNewBoard({
  boardData,
  setBoardData,
}) {
  const [isFormOpen, setIsFormOpen] = useState(false);
  const [boardName, setBoardName] = useState('');

  const cardStyle = {
    maxWidth: 345,
    margin: '0.5rem',
    backgroundColor: 'hsl(209, 23%, 22%)',
    width: '15rem',
  };

  const contentStyle = {
    height: '6rem',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  };

  const nameStyle = {
    color: '#fff',
    fontSize: '10px',
  };

  const handleCreateBoard = () => {
    if (boardName.trim() !== '') {
      axios
        .post(
          `${import.meta.env.VITE_LIST_URL}?name=${boardName}&key=${
            import.meta.env.VITE_API_KEY
          }&token=${import.meta.env.VITE_API_TOKEN}`,
          {
            name: boardName,
          },
        )
        .then((response) => {
          setBoardData([...boardData, response.data]);
          setBoardName('');
        })
        .catch((error) => {
          console.error('An error occurred:', error);
        });
    }
  };

  return (
    <Card style={cardStyle}>
      {isFormOpen ? (
        <CardContent>
          <input
            type="text"
            placeholder="Board Name"
            value={boardName}
            onChange={(e) => setBoardName(e.target.value)}
            style={{ height: '2rem', marginBottom: '10px' }}
          />
          <Button
            variant="contained"
            color="primary"
            onClick={() => {
              handleCreateBoard();
              setIsFormOpen(false);
            }}
          >
            Create
          </Button>
          <Button
            variant="contained"
            color="secondary"
            onClick={() => setIsFormOpen(false)}
          >
            Cancel
          </Button>
        </CardContent>
      ) : (
        <CardActionArea onClick={() => setIsFormOpen(true)}>
          <CardContent style={contentStyle}>
            <Typography variant="h6" component="div" style={nameStyle}>
              Create new board {10 - boardData.length} remaining
            </Typography>
          </CardContent>
        </CardActionArea>
      )}
    </Card>
  );
}

CreateNewBoard.propTypes = {
  noOfBoards: PropTypes.number.isRequired,
  boardData: PropTypes.array.isRequired,
  setBoardData: PropTypes.func.isRequired,
};
