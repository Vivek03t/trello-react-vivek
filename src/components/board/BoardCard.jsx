import React from 'react';
import { Link, useNavigate } from 'react-router-dom';
import Card from '@mui/material/Card';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { CardActionArea } from '@mui/material';
import PropTypes from 'prop-types';

export default function BoardCard({ data }) {
  const navigate = useNavigate();

  const handleBoardClick = () => {
    navigate(`/boards/${data.id}`);
  };

  const nameStyle = {
    position: 'absolute',
    top: '14px',
    left: '14px',
    color: 'white',
    fontSize: '20px',
    fontWeight: '700',
  };

  const cardStyle = {
    maxWidth: 345,
    margin: '0.5rem',
    display: 'inline-block',
    backgroundColor: 'transparent',
  };

  return (
    <>
      <Link to={`/boards/${data.id}`}>
        <Card sx={cardStyle}>
          <CardActionArea onClick={handleBoardClick}>
            <div style={nameStyle}>{data.name}</div>
            <CardMedia
              component={data.prefs.backgroundImageScaled ? 'img' : 'div'}
              height="120"
              src={
                data.prefs.backgroundImageScaled
                  ? data.prefs.backgroundImageScaled[2].url
                  : ''
              }
              alt="background image"
              style={{
                height: '6rem',
                width: '15rem',
                backgroundColor: data.prefs.backgroundColor,
              }}
            />
          </CardActionArea>
          <Typography gutterBottom variant="h6" component="div"></Typography>
        </Card>
      </Link>
    </>
  );
}

BoardCard.propTypes = {
  data: PropTypes.object.isRequired,
  isStarred: PropTypes.bool,
  toggleStarred: PropTypes.func.isRequired,
};
