import React, { useEffect, useState } from 'react';
import axios from 'axios';
import BoardCard from './BoardCard';
import CreateNewBoard from './CreateNewBoard';
import Loader from '/src/components/helper/Loader';

export default function AllBoards() {
  const [boardData, setBoardData] = useState([]);
  const [isLoading, setLoader] = useState(true);
  const [hasError, setError] = useState('');

  useEffect(() => {
    setLoader(true);
    axios
      .get(
        `${import.meta.env.VITE_BASE_URL}key=${
          import.meta.env.VITE_API_KEY
        }&token=${import.meta.env.VITE_API_TOKEN}`,
      )
      .then((response) => {
        setBoardData(response.data);
        setLoader(false);
      })
      .catch((error) => {
        console.error('An error occurred:', error);
        setError(error);
      });
  }, []);

  return (
    <div className="board__container">
      {hasError ? (
        <div className="error-message">
          <p>An error occurred: {hasError.message}</p>
        </div>
      ) : isLoading ? (
        <Loader />
      ) : (
        <>
          {boardData.map((data) => (
            <BoardCard key={data.id} data={data} />
          ))}
          <CreateNewBoard
            boardData={boardData}
            setBoardData={setBoardData}
          />
        </>
      )}
    </div>
  );
}
