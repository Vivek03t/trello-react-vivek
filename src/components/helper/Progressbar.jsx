import * as React from 'react';
import Box from '@mui/material/Box';
import LinearProgress from '@mui/material/LinearProgress';
import { PropTypes } from 'prop-types';

export default function Progressbar({ allCheckitems }) {
  const totalCheckitems = allCheckitems.length;
  const completedCheckitems = allCheckitems.filter(
    (checkitem) => checkitem.state === 'complete',
  ).length;

  const percentage =
    totalCheckitems === 0 ? 0 : (completedCheckitems / totalCheckitems) * 100;

  return (
    <Box sx={{ width: '99%' }}>
      {percentage + '%'}
      <LinearProgress variant="determinate" value={percentage} />
    </Box>
  );
}

Progressbar.propTypes = {
  allCheckitems: PropTypes.array.isRequired,
};
