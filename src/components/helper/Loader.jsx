import React from 'react';

export default function Loader() {
  return (
    <div>
      <img
        className="loader-image"
        src="/src/assets/trello-logo.gif"
        alt="loader-img"
      />
    </div>
  );
}
