import React from 'react';

function Error({ error }) {
  return <h1 className="warning-text">{error}</h1>;
}

export default Error;

