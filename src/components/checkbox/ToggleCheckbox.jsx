import React, { useState } from 'react';
import axios from 'axios';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import { PropTypes } from 'prop-types';

export default function ToggleCheckbox({
  checkitem,
  cardId,
  setAllCheckitems,
}) {
  const { name, id, state } = checkitem;

  const [status, setStatus] = useState(state);

  const handleToggle = (complete) => {
    setStatus(complete ? 'complete' : 'incomplete');

    axios
      .put(
        `${import.meta.env.VITE_CARD_URL}/${cardId}/checkItem/${id}?key=${
          import.meta.env.VITE_API_KEY
        }&token=${import.meta.env.VITE_API_TOKEN}`,
        {
          state: complete ? 'complete' : 'incomplete',
        },
      )
      .then((response) => {
        setAllCheckitems((prevCheckitems) =>
          prevCheckitems.map((checkitem) =>
            checkitem.id === id ? response.data : checkitem,
          ),
        );
      })
      .catch((error) => {
        console.error('An error occurred:', error);
      });
  };

  return (
    <FormGroup>
      <FormControlLabel
        control={
          <Checkbox
            checked={status === 'complete'}
            onChange={(e) => handleToggle(e.target.checked)}
          />
        }
        label={name}
      />
    </FormGroup>
  );
}

ToggleCheckbox.propTypes = {
  name: PropTypes.string.isRequired,
  checkitem: PropTypes.object.isRequired,
  cardId: PropTypes.string.isRequired,
  allCheckitems: PropTypes.array.isRequired,
  setAllCheckitems: PropTypes.func.isRequired,
};
