import React from 'react';
import './App.css';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Home from './components/pages/Home';
import Navbar from './components/navbar/Navbar';
import ListPage from './components/pages/ListPage';

function App() {
  return (
    <BrowserRouter>
      <Navbar />
      <Routes>
        <Route path="/" element={<Home />}></Route>
        <Route path="/boards" element={<Home />}></Route>
        <Route path="/boards/:BoardId" element={<ListPage />}></Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
